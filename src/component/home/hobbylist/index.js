import React from 'react';
import './style.css'

function HobbyList({hobbyList,activeId,onHobbyClick}) {
    const handleClick = (hobby)=>{
        onHobbyClick(hobby)
    }
    return (
        <div>
            <ul>
                {hobbyList.map((item,index)=>{
                    return(
                        <li 
                        key={index}
                        className = {item.id=== activeId ? 'active' :''}
                        onClick = {()=>{handleClick(item)}}
                        >
                            {item.title}
                        </li>
                    )
                })}
            </ul>
        </div>
    );
}

export default HobbyList;