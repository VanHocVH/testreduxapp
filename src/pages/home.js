
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNewHobby, setActiveHobby } from '../actions/hobby';
import HobbyList from '../component/home/hobbylist';

const randomId = ()=>{
    return 1000 + Math.trunc((Math.random()*9000))
}

function Home() {
    const hobbyList = useSelector(state=>state.hobby.list)
    const activeId = useSelector(state=>state.hobby.activeId)

    const dispatch = useDispatch()


    const handleAddHobby = ()=>{
        //new Hobby
        const newId = randomId()
        const newHobby = {
            id:newId,
            title:`hobby ${newId}`
        }
        //dispatch
        const action = addNewHobby(newHobby)
        dispatch(action)
    }
    const handleClick = (hobby)=>{
        dispatch(setActiveHobby(hobby))
    }

    return (
        <div>
            <h1>Hobby List</h1>
            <button onClick={handleAddHobby}>Random Hobby</button>
            <HobbyList 
            hobbyList={hobbyList} 
            activeId = {activeId}
            onHobbyClick={handleClick}
            />
        </div>
    );
}

export default Home;